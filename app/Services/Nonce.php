<?php

namespace App\Services;

class Nonce
{
    public static $nonce;

    public static function nonce()
    {
        if (static::$nonce) {
            return static::$nonce;
        }

        static::$nonce = hash('sha256', csrf_token() . str_random());

        return static::$nonce;
    }
}
