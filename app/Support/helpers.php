<?php

use App\Classes\Page;
use App\Services\Nonce;
use Carbon\Carbon;

function page($view, $title = 'Untitled', $icon = 'fa-file', $description = '')
{
    return new Page($view, $title, $icon, $description);
}

/**
 * Checks whether a value is suitable for use as an ID.
 */
function is_valid_id($val)
{
    return filter_var($val, FILTER_VALIDATE_INT) !== false && $val > 0;
}

/**
 * @return boolean Whether or not we are on a development server.
 */
function is_dev_server()
{
    return strtolower(config('app.env')) != 'production';
}

/**
 * Provides an absolute path to a versioned asset
 */
function cached_asset($path)
{
    try {
        return asset(mix($path, 'dist'));
    } catch (Exception $e) {
        return asset('dist/' . $path);
    }
}

/**
 * Get the config to serialise to JS
 */
function js_config()
{
    return [
        'app' => [
            'name' => config('app.name'),
            'url' => config('app.url'),
        ],
        'formats' => config('constants.Formats'),
        'consts' => [
            
        ]
    ];
}

function nz_date_time($date, $format = 'H:i:s d/m/Y')
{
    return nz_date($date, $format);
}

 /**
 * Convert a db/sql date to a nz date
 */
function nz_date($date, $format = 'd/m/Y')
{
    if ($date instanceof Carbon) {
        return $date->format($format);
    } elseif ($date) {
        return carbon_parse($date)->format($format);
    }
}

/**
 * Helper for parsing a date using carbon.
 */
function carbon_parse($date)
{
    if (!$date) {
        return null;
    }
    return Carbon::parse($date);
}

/**
 * Configures the given builder with WHERE parameter to perform a generic keyword search
 */
function generic_search($builder, $keywords, $columns)
{
    foreach (explode(' ', $keywords) as $keyword) {
        $term = "%$keyword%";
        $builder->where(function ($q) use ($term, $columns) {
            foreach ($columns as $col) {
                $q->where($col, 'ilike', $term);
            }
        });
    }
    return $builder;
}

function sql_from_builder($builder)
{
    $query = $builder->toSql();
    // replace each ? with the binding value.
    foreach ($builder->getBindings() as $binding) {
        $pos = strpos($query, '?');
        if ($pos !== false) {
            $query = substr_replace($query, $binding, $pos, 1);
        }
    }
    return $query;
}

/**
 * Return a unique and random nonce hash.
 *
 * @return string
 */
function nonce()
{
    return Nonce::nonce();
}
