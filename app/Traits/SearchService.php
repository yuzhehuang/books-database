<?php

namespace App\Traits;

use Illuminate\Http\Request;

/**
 * Add this trait to any resource controllers where its model can have full-text searches done by users
 */
trait SearchService
{
    public function search(Request $request)
    {
        $pageSize = 20;
        $offset = $pageSize * ($request->input('page', 1) - 1);
        $modelClass = 'App\Models\\' . str_replace('Controller', '', class_basename(get_class($this)));

        if ($request->term) {
            return $modelClass::search($request->term)
                ->orderBy('name')
                ->orderBy('id')
                ->paginate($pageSize);
        } else {
            return $modelClass::orderBy('name')
                ->orderBy('id')
                ->paginate($pageSize);
        }
    }
}
