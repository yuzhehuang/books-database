<?php

namespace App\Models;

use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * publisher books
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }

    /**
     * scope to perform generic search on publisher
     */
    public static function scopeSearch($b, $keywords)
    {
        return generic_search($b, $keywords, ['name']);
    }
}
