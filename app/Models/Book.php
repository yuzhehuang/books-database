<?php

namespace App\Models;

use App\Models\Author;
use App\Models\Category;
use App\Models\Publisher;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use Sluggable;

    protected $fillable = [
        'title',
        'sub_title',
        'description',
        'isbn',
        'published_date',
        'price',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['publisher.name', 'title'],
            ],
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    /**
     * The categories that the book is under
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'book_categories');
    }

    /**
     * The authors who wrote the book
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'book_authors');
    }

    /**
     * Retrieves a listing of books for use in a table or similar. Joins some required
     * tables that are useful when viewing details of a lot of books.
     * @return QueryBuilder A query builder that you can then use to apply additional
     *  filters.
     */
    public function scopeListing($q)
    {
        $q->select([
            'books.*',
            'p.name as publisher_name'
        ])
            ->leftJoin('publishers as p', 'p.id', '=', 'books.publisher_id');
    }
}
