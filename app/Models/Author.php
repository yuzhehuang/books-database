<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * scope to perform generic search on publisher
     */
    public static function scopeSearch($b, $keywords)
    {
        return generic_search($b, $keywords, ['name']);
    }
}
