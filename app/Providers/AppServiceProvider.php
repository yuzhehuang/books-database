<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCustomValidators();
    }

    private function registerCustomValidators()
    {
        // Custom validation rule for checking if a parameter is a valid ID.
        Validator::extend('valid_id', function ($attribute, $value, $parameters, $validator) {
            return is_valid_id($value);
        });
    }
}
