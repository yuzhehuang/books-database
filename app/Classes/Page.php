<?php

namespace App\Classes;

use Illuminate\Contracts\Support\Responsable;

/**
 * Describes the details of an individual page being loaded.
 *
 * Page classes are used to ensure we load all the common data required for
 * each page in a consistent manner.
 */
class Page implements Responsable
{
    /**
     * The path to the view file.
     *
     * @var string
     */
    protected $view;

    /**
     * The data to be available in the view
     *
     * @var array
     */
    protected $data;

    /**
     * The title of the page.
     *
     * @var string
     */
    protected $title;

    /**
     * The page icon.
     *
     * @var string
     */
    protected $icon;

    /**
     * The description of the page.
     *
     * @var string
     */
    protected $description;

    /**
     * Determines whether a title should be displayed on the page. Useful for things like home etc.
     *
     * @var boolean
     */
    protected $showTitle = true;

    /**
     * Configure the page object.
     *
     * @return void
     */
    public function __construct($view, $title = 'Untitled', $icon = 'fa-file', $description = '')
    {
        $this->view = $view;
        $this->title = $title;
        $this->icon = $icon;
        $this->description = $description;
    }

    /**
     * Create the actual view response to send to the client.
     */
    public function toResponse($request)
    {
        return $this->view(
            array_merge([
                'user' => auth()->user(),
                'page' => $this->get(),
                'viewmodel' => $this->getViewModel(),
            ], (array) $this->data)
        );
    }

    /**
     * Set the data for the view
     *
     * @return $this
     */
    public function with($data)
    {
        $this->data = $data;
        return $this;
    }

    public function withDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Attaches the viewmodel associated to the page.
     *
     * @return string|void
     */
    protected function getViewModel()
    {
        $path = 'js/viewmodels/' . str_replace('.', '/', $this->view) . '.js';

        if (file_exists(public_path('dist/' . $path))) {
            return $path;
        }
    }

    /**
     * Get the common page data to send to the client.
     *
     * @return array
     */
    protected function get()
    {
        return [
            'title' => $this->title,
            'icon' => $this->icon,
            'description' => $this->description,
            'previousUrl' => url()->previous(),
            'showTitle' => $this->showTitle,
        ];
    }

    /**
     * Create the view response.
     *
     * @var string
     */
    protected function view($data)
    {
        return response()->view($this->view, $data)
            ->header('Cache-Control', 'no-transform')
            ->header('Cache-Control', 'no-store');
    }
}
