<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Attaches various headers to all outgoing responses.
 */
class AttachHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (!method_exists($response, 'header')) {
            return $next($request);
        }

        $this->attachCspHeader($request, $response);
        $this->attachOtherHeaders($request, $response);

        return $response;
    }

    /**
     * Attaches the required CSP headers to help prevent remote injection and execution of js (XSS) and css.
     */
    private function attachCspHeader(Request $request, $response)
    {
        if ($request->ajax() || config('app.debug')) {
            return;
        }

        $cspStyleAndFontExceptions = collect([
            'https://fonts.gstatic.com',
            'https://fonts.googleapis.com'
        ])->implode(' ');

        $csp = [
            sprintf("script-src 'self' 'nonce-%s'", nonce()),
            sprintf("style-src 'self' %s", $cspStyleAndFontExceptions),
            sprintf("font-src 'self' %s", $cspStyleAndFontExceptions),
            "object-src 'none'",
            "base-uri 'none'",
        ];

        $response->header("Content-Security-Policy", implode(';', $csp));
        return $response;
    }

    private function attachOtherHeaders(Request $request, $response)
    {
        // no-store - prevent form data from being stored and re-retrieved
        // when the back button is used to go back to a form
        $response->header('Cache-Control', 'no-store');

        // stops pages from loading when detect reflected cross-site scripting (XSS) attacks
        $response->header('X-XSS-Protection', '1');

        //The page cannot be displayed in a frame, regardless of the site attempting to do so.
        $response->header('X-Frame-Options', 'DENY');

        // Force HTTPS for a year including sub domains.
        $response->header('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload');

        // Prevents browsers from using MIME-analysis to reduce risk of uploaded content
        // to be interpreted as an executable or HTML.
        $response->header('X-Content-Type-Options', 'nosniff');
    }
}
