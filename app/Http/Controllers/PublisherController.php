<?php

namespace App\Http\Controllers;

use App\Traits\SearchService;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    use SearchService;
}
