<?php

namespace App\Http\Controllers;

use App\Classes\Page;
use App\Http\Requests\BookSearchRequest;
use App\Models\Book;
use App\Models\Category;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return page('books.table', 'Search Books', 'fa-search', 'A place where you can find the books you like')
            ->with([
                'categories' => Category::orderBy('name')->get(),
            ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        return page('books.show', 'Book Details', 'fa-book')
            ->with([
                'book' => $book,
                'categories' => $book->categories,
                'authors' => $book->authors,
                'publisher' => $book->publisher,
            ]);
    }

    /**
     * Retrieves a list of books for the datatable.
     * @param  BookSearchRequest $request
     * @return Illuminate\Support\Collection
     */
    public function datatable(BookSearchRequest $request)
    {
        $builder = Book::listing()
            ->with(['authors', 'categories']);
        $request->applySearchFilters($builder);
        return datatables()
            ->of($builder)
            ->toJson();
    }
}
