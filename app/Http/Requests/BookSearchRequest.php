<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'nullable|valid_id',
            'author_id' => 'nullable|valid_id',
            'publisher_id' => 'nullable|valid_id',
            'published_year' => 'nullable|integer',
            'published_month' => 'nullable|integer',
        ];
    }

    /**
     * Pull the filters out of this request and apply to the given search query builder
     */
    public function applySearchFilters($builder)
    {
        if ($this->category_id) {
            $builder->whereHas('categories', function ($c) {
                $c->where('categories.id', $this->category_id);
            });
        }

        if ($this->author_id) {
            $builder->whereHas('authors', function ($a) {
                $a->where('authors.id', $this->author_id);
            });
        }

        if ($this->publisher_id) {
            $builder->where('publisher_id', $this->publisher_id);
        }

        if ($this->published_year) {
            $builder->whereRaw("date_part('year', published_date) = '$this->published_year'");
        }
        
        if ($this->published_month) {
            $builder->whereRaw("date_part('month', published_date) = '$this->published_month'");
        }
    }
}
