<?php

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Book::class, 1000)->create()->each(function ($book) {
            // Seed the relationships
            $categoryIds = Category::inRandomOrder()->select('id')->take(5)->get()->pluck('id');
            $book->categories()->sync($categoryIds);

            $authorIds = Author::inRandomOrder()->select('id')->take(2)->get()->pluck('id');
            $book->authors()->sync($authorIds);
        });
    }
}
