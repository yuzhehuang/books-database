<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Publisher;
use Faker\Generator as Faker;

$factory->define(App\Models\Book::class, function (Faker $faker) {
    $publisherIds = Publisher::pluck('id');
    return [
        'title' => $faker->word(),
        'sub_title' => $faker->text(100),
        'description' => $faker->text(500),
        'isbn' => $faker->isbn13(),
        'publisher_id' => $publisherIds->random(),
        'published_date' => $faker->date(),
        'price' => $faker->randomNumber(2),
    ];
});
