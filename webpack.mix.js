const mix = require('laravel-mix')
const webpack = require('webpack')
var glob = require('glob')
const path = require('path')
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

mix
    .setResourceRoot('../')
    .setPublicPath('public/dist/')
    .webpackConfig({
        resolve: {
            modules: [
                path.resolve('./resources/assets/js'),
                path.resolve('./resources/assets/sass'),
                path.resolve('./node_modules')
            ]
        },
        plugins: [
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            new webpack.IgnorePlugin(/^\.\/languages$/, /numbro$/),
            // new BundleAnalyzerPlugin(),
            new LodashModuleReplacementPlugin({
                // https://github.com/lodash/lodash-webpack-plugin#feature-sets
                shorthands: true,
                collections: true,
                paths: true
            }),
        ],
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    exclude: [
                        /node_modules/,
                        /modernizrCustom\.js/
                    ],
                    loader: 'eslint-loader',
                    options: {
                        emitWarning: true
                    }
                },
            ],
        },
    })

for (let file of glob.sync('resources/assets/js/viewmodels/**/*.js')) {
    mix.js(file, 'public/dist/' + file.slice(file.indexOf('js/viewmodels')))
}

mix
    .copy('resources/assets/images', 'public/dist/images')
    .js('resources/assets/js/app.js', 'public/dist/js')
    .sass('resources/assets/sass/app.scss', 'public/dist/css')
    .extract()
    .browserSync({
        proxy: 'http://books.local:8080',
        files: [
            'public/dist/css/**/*.css',
            'public/dist/js/**/*.js',
            'resources/views/**/*.php'
        ]
    })
    .version()