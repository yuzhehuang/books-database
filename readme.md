![alt text](/resources/assets/images/favicons/android-icon-192x192.png "Bookings Database")

# Books database
> A web-based app that allows user to search for books, explore the search results and see further details about the books.
> This project is built using Laravel 6.1. Server Requirements and documentation: https://laravel.com/docs/6.x

## Getting started for local development
* Make sure you have composer and yarn installed and globally available on your PATH.
* After cloning the repo, checkout the `develop` branch. Work should ideally be done in develop then when ready to go live, merge develop into master with a non-fastforward merge (on master: `git merge --no-ff develop`). This helps keep master clean so you can more easily see when  updates have been deployed. For more info see the gitflow workflow.
* Run `composer install` to install all php dependencies such as laravel.
* Copy `.env.example` to `.env` and set the config values for your environment accordingly.
* Create a blank postgres database and enter its connection details into your `.env` file
* Run `php artisan key:generate` to generate your own encryption key.
* Run `php artisan migrate --seed` to create the required database tables and seed it.
* Run `yarn` to install all client-side dependencies.
* Use `npm run watch` to run the default laravel mix configured watch task. This provides live reloading (browsersync) of local files as you work on them.
* The app is designed for serving under `http://books.local` make sure you have `books.local` in your hosts file pointing to `127.0.0.1` and a vhost enabled with the following settings below

## Other notes
### reset local database
* Run `php artisan migrate:fresh --seed`.

### Example vhost settings:
```
<VirtualHost *:8080>
    ServerAdmin YOUR-EMAIL
    DocumentRoot "C:/xampp/htdocs/books/public"
    ServerName books.local
</VirtualHost>
```

## Deployment Notes

### if deploying the first time
* Create a new database login role for this app and genrate a random secure password
* Make the new login role the owner of the database, schema and grant the right permissions to the tables, views and etc.

### Otherwise
* Cache busting is automatic as 
* Commit your changes to develop, merge to master and push. Use a non fast forward merge to retain history of the point the branches were merged `git merge --no-ff develop`.
* Deploy the code as per usual
* Re-cache config on production using `php artisan config:cache`
* Run migration `php artisan migrate`
* Run seeder if needed `php artisan db:seed --class=<YOURSEEDERNAME>`
* Restart worker `supervisorctl restart <YOURWORKER>`