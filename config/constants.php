<?php

/*
 * Holds all our custom application constants.
 */
return [
    'DEV_NAME' => env('DEV_NAME', 'Nick Huang'),
    'DEV_EMAIL' => env('DEV_EMAIL', 'hyz326@hotmail.com'),

    // Holds various formats to use for different libraries.
    'Formats' => [
        'Carbon' => [
            'TIME' => 'h:i:s A',
            'DATE' => 'd/m/Y',
            'DATETIME' => 'h:i:s A d/m/Y',
        ],
        'Moment' => [
            'TIME' => 'h:mm A',
            'TIME_SQL' => 'HH:mm',
            'DATE' => 'DD/MM/YYYY',
            'DATE_SQL' => 'YYYY-MM-DD',
            'DATETIME' => 'h:mm A DD/MM/YYYY',
            'DATETIME_SQL' => 'YYYY-MM-DD HH:mm:ss',
        ],
    ],
];
