@extends ('layouts.default')

@section ('content')

@include ('includes.title')

<div class="row">
    <div class="col-xl-8">
        <div class="row">
            <div class="col-xl-4">
                <img src="{{ cached_asset('images/book-cover.jpg') }}" alt="{{ $book->title }}" class="img-fluid">
            </div>
            <div class="col-xl-8">
                <div class="form-group">
                    <label>Title</label>
                    <div>{{ $book->title }}</div>
                </div>
                <div class="form-group">
                    <label>Sub Title</label>
                    <div>{{ $book->sub_title }}</div>
                </div>
                <div class="form-group">
                    <label>Categories</label>
                    <div>
                        @foreach($categories as $category)
                            <span class="badge badge-info">{{ $category->name }}</span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label>Authors</label>
                    <div>{{ $authors->implode('name', '; ') }}</div>
                </div>
                <div class="form-group">
                    <label>Publisher</label>
                    <div>{{ $publisher->name }}</div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <div>{{ $book->description }}</div>
                </div>
                <div class="form-group">
                    <label>ISBN</label>
                    <div>{{ $book->isbn }}</div>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <div>${{ number_format($book->price, 2)}}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
