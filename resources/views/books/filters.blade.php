<form @submit.prevent="search" class="mt-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="author_id">By Author</label>
                <lookup v-model="filters.author_id" :map-results="mapUserResults" endpoint="authors/search" id="author_id" placeholder="Any"></lookup>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="publisher_id">By Publisher</label>
                <lookup v-model="filters.publisher_id" :map-results="mapUserResults" endpoint="publishers/search" id="publisher_id" placeholder="Any"></lookup>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <label for="category_id">By Category</label>
                <select v-model="filters.category_id" id="category_id" class="form-control">
                    <option :value="null">Any</option>
                    <option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="published_year">By Published Year</label>
                <select v-model="filters.published_year" id="published_year" class="form-control">
                    <option :value="null">Any</option>
                    <option v-for="year in publishedYears">@{{ year }}</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="published_month">By Published Month</label>
                <select v-model="filters.published_month" id="published_month" class="form-control">
                    <option :value="null">Any</option>
                    <option v-for="month in publishedMonths" :value="month.value">@{{ month.name }}</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>&nbsp;</label>
                <div>
                    <button @click.prevent="resetFilters" class="btn btn-outline-secondary"><span class="fa fa-times"></span> Clear Filters</button>
                </div>
            </div>
        </div>
    </div>
    <hr>
</form>
