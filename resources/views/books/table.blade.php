@extends ('layouts.default')

@section ('content')

@include ('includes.title')

@include('books.filters')

<table ref="datatable" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Sub Title</th>
            <th>Categories</th>
            <th>Authors</th>
            <th>Publisher</th>
            <th>Price</th>
        </tr>
    </thead>
</table>

@stop
