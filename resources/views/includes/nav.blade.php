<b-navbar id="page-nav" toggleable="lg" type="dark" variant="dark" sticky>
	<b-navbar-brand href="{{ url('/') }}" class="d-flex align-items-center">
		<img src="{{ cached_asset('images/favicons/favicon-32x32.png') }}" width="32" height="32" class="d-inline-block align-top mr-2">
		{{ config('app.name') }}
	</b-navbar-brand>

	<b-navbar-toggle target="app-navbar"></b-navbar-toggle>

	<b-collapse is-nav id="app-navbar">
		<b-navbar-nav>
			<b-nav-item href="{{ url('/') }}">
				<span class="fa fa-search"></span> Search
			</b-nav-item>
		</b-navbar-nav>

		<b-navbar-nav class="ml-auto">
			<b-nav-text><span class="fa fa-user"></span> Guest</b-nav-text>
		</b-navbar-nav>
	</b-collapse>
</b-navbar>
