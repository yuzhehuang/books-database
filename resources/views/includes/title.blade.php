<div class="page-header">
	<h1>
        @if (isset($page['icon']))
        <span class="page-icon fa {{ $page['icon'] }}"></span>
        @endif
        {{ $page['title'] }}
    </h1>
    @isset($page['description'])
        <p class="m-0 text-muted">{{ $page['description'] }}</p>
    @endif
</div>
