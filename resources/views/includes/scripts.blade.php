<script nonce="{{ nonce() }}">
	var globals = {!! json_encode(js_config()) !!};
    globals.user = {!! json_encode($user ?: new stdClass) !!};
</script>

<script nonce="{{ nonce() }}" src="{{ cached_asset('js/manifest.js') }}"></script>
<script nonce="{{ nonce() }}" src="{{ cached_asset('js/vendor.js') }}"></script>
<script nonce="{{ nonce() }}" src="{{ cached_asset('js/app.js') }}"></script>

{{-- Load any page-specific viewmodel if available --}}
@if (isset($viewmodel))
<script nonce="{{ nonce() }}" src="{{ cached_asset($viewmodel) }}"></script>
@endif

@php
	$filteredData = array_except(get_defined_vars(), [
		'__path', '__data', 'obLevel', '__env', 'app', 'errors', 'page', 'viewmodel'
	]);
@endphp

{{-- Stack for any extra page-specific js scripts --}}
@stack('js')

{{-- Viewmodel autoloader --}}
<script nonce="{{ nonce() }}">
	$(document).ready(function() {
		// Try load the page-specific viewmodel if one available
		let vmClass = typeof ViewModel === 'function' ? ViewModel : Vue;
		const app = new vmClass({el: '#app', data: function() {
			return {!! json_encode($filteredData ?: new stdClass) !!}
		}});

		// Fade in to prevent flash of unstyled content
		setTimeout(function() {
			$('#app').removeClass('loading');
		});
	});
</script>
