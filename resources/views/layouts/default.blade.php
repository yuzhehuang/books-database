<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="csrf-token" value="{{ csrf_token() }}">

        @include ('includes.favicons')

        <title>{{ !empty($page['title']) ? ($page['title'] . ' - ') : '' }} {{ config('app.name') }}</title>

        <link rel="stylesheet" href="{{ cached_asset('css/app.css') }}">

        @include ('includes.scripts')
    </head>
    <body>

        <div id="app" class="loading">
            @include ('includes.nav')
            <div id="page-content" style="min-height: 80vh">
                @yield ('content')
            </div>

            <footer class="text-center mb-4 mt-4">
                <img src="{{ cached_asset('images/logo.png') }}" alt="Logo">
                <p class="mt-2 text-muted">
                    <small>Developed and maintained by <a href="mailto:{{ config('constants.DEV_EMAIL') }}">{{ config('constants.DEV_NAME') }}</a></small>
                </p>
            </footer>
        </div>
    </body>
</html>
