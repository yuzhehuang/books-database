import { trim } from 'lodash-es'

window.Vue = require('vue')
Vue.config.productionTip = false

///////////////////////////
// Custom Vue Components //
///////////////////////////

Vue.component('lookup', require('components/AjaxLookup.vue').default)

//////////////////////////////
// Bootstrap Vue Components //
//////////////////////////////

import { VBTooltipPlugin, NavbarPlugin, TabsPlugin } from 'bootstrap-vue'
Vue.use(NavbarPlugin)
Vue.use(VBTooltipPlugin)
Vue.use(TabsPlugin)

////////////////////
// Custom Filters //
////////////////////

Vue.filter('date', value => helpers.string.asDate(value, globals.formats.Moment.DATE))
Vue.filter('time', (value, parseFormat) => helpers.string.asDate(value, globals.formats.Moment.TIME, parseFormat))
Vue.filter('datetime', value => helpers.string.asDate(value, globals.formats.Moment.DATETIME))
Vue.filter('2dp', value => numbro(value).format('0.00'))
Vue.filter('4dp', value => numbro(value).format('0.0000'))
Vue.filter('bytes', (value, format) => numbro(value).format(format || '0.0b'))
Vue.filter('dash', value => !value ? '-' : value)
Vue.filter('trim', value => trim(value))
Vue.filter('default', (value, defaultVal) => value || defaultVal)
