import { get, isObject, isString, join, startsWith, values } from 'lodash-es'

/**
 * Global application helper functions
 */
export default {
    string: {
        /**
         * Helper for trying to convert a date value (either falsy, string-based or moment)
         * into the given format string.
         */
        asDate(value, format, parseFormat = null) {
            if (!value) return ''
            return value instanceof moment
                ? value.format(format)
                : moment(value, parseFormat).format(format)
        },
        asRelativeTimestamp(value) {
            if (!value) return ''
            return value instanceof moment
                ? value.fromNow()
                : moment(value).fromNow()
        },
        asRelativeDate(value) {
            if (!value) return ''
            var mmt = value instanceof moment
                ? value
                : moment(value).startOf('day')

            var days = mmt.diff(moment().startOf('day'), 'days')
            switch (days) {
            case 0: return 'today'
            case 1: return 'tomorrow'
            case -1: return 'yesterday'
            default:
                return moment.duration(days, 'days').humanize(true)
            }
        },
        asPeriod(value) {
            if (!value) return ''
            return value instanceof moment
                ? value.format('MMM YYYY')
                : moment(value, 'YYYYMM').format('MMM YYYY')
        },
        asCurrency(value) {
            if (!value) return '$0.00'
            return numbro(value).format('$0,000.00')
        },
    },

    util: {
        onAjaxError(error) {
            let httpStatus = error.response.status
            let responseData = error.response.data
            let showError = (heading, error) => {
                Swal.fire({title: heading, html: error, type: 'error'}).finally(() => {
                    let redirect = get(responseData, 'redirect') || get(responseData, 'message.redirect')
                    if (redirect) {
                        location.href = redirect
                    }
                })
            }

            // Was it an XHR abort? Dont need to handle these.
            if (httpStatus === 0) return

            if (httpStatus === 403) {
                return showError('Permission Denied', 'You are not allowed to perform that action.')
            }

            if (httpStatus === 404) {
                return showError('Oops!', 'A page or resource you requested could not be found. Refresh the page and try again.')
            }

            // No specific response returned / internal server error. Just show general error message.
            if (httpStatus === 500) {
                return showError('Oops!', 'Something went wrong, please try again later')
            }

            // Laravel validation error messages
            if (isObject(responseData.errors)) {
                var validation = join(values(responseData.errors), '<br>')
                return showError('Oops!', validation)
            }

            // Custom server error messages - These are simply stored in a object with a message key.
            if (responseData.message) {
                var message = responseData.message
                if (isObject(message)) {
                    return showError(message.heading, message.error)
                } else {
                    return showError('Oops!', message)
                }
            }

            if (isString(responseData)) {
                // Plain string validation message
                return showError('Oops!', responseData)
            } else if (responseData.exception) {
                // Laravel internal error with no specific message, just echo the exception string.
                return showError('Oops!', responseData.exception)
            }

            return showError('Oops!', 'Something went wrong, please try again later')
        }
    },

    html: {
        link(url, name, cls = '', icon = '', target = '', tooltip = '') {
            // uri segment?
            if (!startsWith(url, 'http')) {
                url = globals.app.url + '/' + url
            }
            icon = icon ? `<span class="fa fa-fw ${icon}"></span> ` : ''
            return `<a class="${cls}" target="${target || ''}" ${tooltip ? 'data-tooltip' : ''} ${tooltip ? `title="${tooltip}"` : ''} href="${url}">${icon}${name}</a>`
        },
        mailTo(email, subject) {
            return `<a href="mailto:${email}${subject ? `?subject=${subject}` : ''}">${email}</a>`
        },
        label(cls, text = '', icon = '', title = '') {
            icon = icon ? `<span class="fa fa-fw ${icon}"></span> ` : ''
            return `<span title="${title}" class="badge ${cls}">${icon}${text}</span>`
        },
        button(cls, text = '', icon = '', tooltip = '') {
            icon = icon ? `<span class="fa fa-fw ${icon}"></span> ` : ''
            return `<button type="button" ${tooltip ? 'data-tooltip' : ''} ${tooltip ? `title="${tooltip}"` : ''} class="btn ${cls}">${icon}${text}</button>`
        },
        nl2br(str, isXHTML) {
            var breakTag = (isXHTML || typeof isXHTML === 'undefined') ? '<br />' : '<br>'
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
        }
    }
}
