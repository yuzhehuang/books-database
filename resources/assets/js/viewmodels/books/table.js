import DatatableColumnBuilder from 'datatableColumnBuilder'
import NamedValue from 'namedValue'
import { map, range, each, omitBy, isNil, join } from 'lodash-es'
class Viewmodel extends Vue {
    constructor(config) {
        super({
            ...config,

            data() {
                var pageData = config.data()
                return {
                    categories: pageData.categories,
                    filters: {
                        category_id: null,
                        author_id: null,
                        publisher_id: null,
                        published_year: null,
                        published_month: null,
                    },
                    publishedYears: range(parseInt(moment().add(1, 'year').format('YYYY')), 1899),
                    publishedMonths: range(1, 13).map(val => {
                        var id = moment(val, 'M').format('M')
                        var month = moment(val, 'MM').format('MMMM')
                        return new NamedValue(month, id)
                    }),
                }
            },

            methods: {
                // customised function to remap author/publisher name search results
                mapUserResults(results) {
                    return map(results, data => {
                        data.text = data.name
                        return data
                    })
                },

                // reset all the selected filters
                resetFilters() {
                    each(this.filters, (filter, key) => this.filters[key] = null)
                },

                // function to map filters data before sending
                getFiltersAsKeyValuePairs() {
                    var sendData = {}
                    each(omitBy(this.filters, filter => isNil(filter)), (filter, key) => {
                        switch (typeof filter) {
                            case 'object':
                                // assumes filter objects are keyed by ID
                                sendData[key] = filter.id
                                break
                            default:
                                sendData[key] = filter
                        }
                    })
                    return sendData
                },

                // function to refresh the datatables. Auto refresh on page load and when filter changes
                refreshDatatable() {
                    if (this.datatable) {
                        return this.datatable.ajax.reload()
                    }

                    this.datatable = $(this.$refs.datatable).DataTable({
                        processing: true,
                        serverSide: true,
                        pageLength: 10,
                        order: [[0, 'desc']],
                        ajax: {
                            url: `${globals.app.url}/books/datatable`,
                            data: sendData => ({
                                ...sendData,
                                ...this.getFiltersAsKeyValuePairs()
                            })
                        },
                        columns: new DatatableColumnBuilder()
                            .visible(false) // show id column
                            .column('title')
                                .searchable()
                                .renderAsLink(row => `books/${row.slug}`)
                            .column('sub_title')
                                .searchable()
                            .column('categories')
                                .render((data, type, row) => {
                                    return join(map(row.categories, (category) => {
                                        return helpers.html.label('badge-info', category.name)
                                    }), ' ') 
                                })
                            .column('authors')
                                .render((data, type, row) => {
                                    return join(map(row.authors, (author) => {
                                        return author.name
                                    }), '; ') 
                                })
                            .column('p.name', 'publisher_name')
                                .searchable()
                            .column('price')
                                .searchable()
                            .get()
                    })
                }
            },

            watch: {
                filters: {
                    deep: true,
                    handler() {
                        this.refreshDatatable()
                    }
                }
            },

            mounted() {
                this.refreshDatatable()
            }
        })
    }
}

window.ViewModel = Viewmodel
