import "core-js/stable"
import "regenerator-runtime/runtime"

window.$ = window.jQuery = require('jquery')
window.moment = require('moment')
window.numbro = require('numbro')
window.Swal = require('sweetalert2')

window.axios = require('axios')

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
let token = document.head.querySelector('meta[name="csrf-token"]')
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
window.axios.interceptors.response.use(response => response, (error) => {
    window.helpers.util.onAjaxError(error)
    throw error // rethrow error to ensure any further promise chain is rejected.
})

require('datatables.net-responsive-bs4')
$.fn.dataTable.defaults = {
    ...$.fn.dataTable.defaults,
    dom: "<'row'<'col-md-10'fr><'col-md-2'l>><'row'<'col-md-12't>><'row'<'col-md-6'i><'col-md-6'p>>",
    serverSide: true,
    pageLength: 25,
    processing: true,
    responsive: true,
    autoWidth: false, // true = prevents table being 100% of its container upon window resize
    searchDelay: 600,
    language: {
        lengthMenu: "Show _MENU_ rows",
        search: '<span class="fa fa-search"></span> ',
        searchPlaceholder: 'Search records',
        processing: 'Loading...',
    },
}

require('./vue')

window.helpers = require('./helpers').default

require('tempusdominus-bootstrap-4')
$.fn.datetimepicker.Constructor.Default.icons = {
    time: 'fa fa-clock',
    date: 'fa fa-calendar',
    up: 'fa fa-arrow-up',
    down: 'fa fa-arrow-down',
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-calendar-check-o',
    clear: 'fa fa-trash',
    close: 'fa fa-times'
}