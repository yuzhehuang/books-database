/**
 * Simple name-value pair
 */
export default class NamedValue {

    constructor(name, value) {
        this.name = name
        this.value = value
    }

}
