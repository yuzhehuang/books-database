import { isNil, truncate, isFunction } from 'lodash-es'

/**
 * Utility class for fluently building the column definitions required by datatables
 */
export default class DatatableColumnBuilder {

    constructor() {
        this.columns = []
        this.activeColumn = null
        this.sortingEnabled = true

        // Always include the ID column by default
        this.column('id').visible(false).sortable().searchable()
    }

    /**
     * Create a new column and set it as the active chained column
     */
    column(name, data) {
        let column = new DatatableColumn(name, data)
        this.activeColumn = column
        this.columns.push(column)

        if (!this.sortingEnabled) {
            this.unsortable()
        }

        return this
    }

    /**
     * Disables sorting for any columns next added
     */
    disableSorting() {
        this.sortingEnabled = false
        return this
    }

    /**
     * Set column responsive priority.
     * https://datatables.net/extensions/responsive/priority
     */
    priority(val) {
        this.activeColumn.responsivePriority = val
        return this
    }

    /**
     * Enable searching for column
     */
    searchable() {
        this.activeColumn.searchable = true
        return this
    }

    /**
     * Set the visible state based on a condition
     */
    visible(bool) {
        this.activeColumn.visible = !!bool
        return this
    }

    /**
     * Disable sorting for column
     */
    unsortable() {
        this.activeColumn.sortable = false
        return this
    }

    /**
     * Enable sorting for column
     */
    sortable() {
        this.activeColumn.sortable = true
        return this
    }

    /**
     * Set column rendering function to use
     */
    render(func) {
        this.activeColumn.render = func
        return this
    }

    renderAsTruncatedText(maxLength = 40) {
        return this.render(data => {
            return `<span title="${data}">${truncate(data, {length: maxLength})}</span>`
        })
    }

    renderAsNumber(decimals) {
        return this.render(data => {
            if (isNil(data)) return ''
            return numbro(data).format(decimals ? '0,0.00' : '0,0')
        })
    }

    renderAsCurrency() {
        return this.render(data => {
            if (isNil(data)) return ''
            return numbro(data).format('$0,0.00')
        })
    }

    /**
     * Render a download button. Defaults to download a media item based on row data
     */
    renderAsDownloadButton(downloadUri) {
        return this.render((data, type, row) => {
            if (data || downloadUri) {
                return helpers.html.link(`${globals.app.url}/${downloadUri ? downloadUri : `plans/${row.plan_id}/media/${data}/download`}`, 'Download', 'btn btn-outline-info', 'fa-download')
            }
            return ''
        })
    }

    /**
     * Render column data as a label
     */
    renderAsLabel(labelCls, iconCls) {
        return this.render((data, type, row) => {
            if (!data) return ''

            // Evalulate label class from row data?
            var cls = isFunction(labelCls)
                ? labelCls(row)
                : labelCls

            // Evalulate icon class from row data?
            var icon = isFunction(iconCls)
                ? iconCls(row)
                : iconCls

            return helpers.html.label(cls, data, icon)
        })
    }

    /**
     * Render column data as a label
     */
    renderAsLink(urlGenerator, defaultVal = '') {
        return this.render((data, type, row) => {
            let link = urlGenerator(row)
            return link ? helpers.html.link(`${globals.app.url}/${link}`, data) : data || defaultVal
        })
    }

    renderAsMailTo(subject = '') {
        return this.render(email => {
            var link = 'mailto:' + email
            if (subject) {
                link += '?subject=' + subject
            }
            return helpers.html.link(link, email)
        })
    }

    renderAsBoolean() {
        return this.render(data => `<span class="fa fa-fw ${data ? 'fa-check' : 'fa-times'}"></span>`)
    }

    renderAsDate(defaultVal = '') {
        return this.render((data) => data ? moment(data).format(globals.formats.Moment.DATE) : defaultVal)
    }

    renderAsRelativeDate(defaultVal = '') {
        return this.render((data) => {
            if (!data) return defaultVal
            var date = moment(data)
            var today = moment().startOf('day')
            var timeAgo = ''
            switch (date.diff(today, 'days')) {
            case -1: timeAgo = 'Yesterday'; break
            case 0: timeAgo = 'Today'; break
            case 1: timeAgo = 'Tomorrow'; break
            default: timeAgo = date.from(today); break
            }
            return `<abbr title="${date.format(globals.formats.Moment.DATE)}">${timeAgo}</abbr>`
        })
    }

    renderAsTimestamp(relative = true, defaultVal = '') {
        return this.render(data => {
            if (!data) return defaultVal
            var full = moment(data).format(globals.formats.Moment.DATETIME)
            return relative
                ? `<abbr title="${full}">${moment(data).fromNow()}</abbr>`
                : full
        })
    }

    /**
     * Set the order data field for the column
     */
    orderData(data) {
        this.activeColumn.orderData = data
        return this
    }

    /**
     * Set column custom class name
     */
    className(cls) {
        this.activeColumn.className = cls
        return this
    }

    /**
     * Set the default value to use for a column
     */
    default(value) {
        this.activeColumn.defaultContent = value
        return this
    }

    /**
     * Creates a special column used for row action buttons
     */
    actionsColumn(name = 'id') {
        return this.column(name)
            .unsortable()
            .className('actions')
    }

    /**
     * Return all columns that have been built
     */
    get() {
        return this.columns
    }

}

class DatatableColumn {

    constructor(name, data) {
        this.name = name
        this.data = data || name
        this.searchable = false
        this.sortable = true
        this.visible = true
        this.render = undefined
        this.className = undefined
        this.defaultContent = undefined
    }

}
