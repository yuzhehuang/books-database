<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index')->name('home');
Route::get('books/datatable', 'BookController@datatable');
Route::get('books/{book}', 'BookController@show');
Route::get('authors/search', 'AuthorController@search');
Route::get('publishers/search', 'PublisherController@search');
